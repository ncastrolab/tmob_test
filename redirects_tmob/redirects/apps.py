from django.apps import AppConfig


class RedirectsConfig(AppConfig):
    name = 'redirects'

    def ready(self): #method just to import the signals
    	import redirects.signals
