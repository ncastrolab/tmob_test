from django.db import models

from redirects.signals import toggle_redirect
from redirects.utils import now


class Redirect(models.Model):
    key = models.CharField(max_length=200, unique=True)
    url = models.CharField(max_length=200)
    active = models.BooleanField(default=False)
    created_at = models.DateTimeField(default=lambda: now(), editable=False)
    updated_at = models.DateTimeField(editable=False)

    def save(self, *args, **kwargs):
        """ On save, update timestamps """
        self.updated_at = now()
        toggle_redirect.send(sender=self.__class__, redirect=self)
        return super(Redirect, self).save(*args, **kwargs)

    def custom_values(self):
        return {"key": self.key, "url": self.url}