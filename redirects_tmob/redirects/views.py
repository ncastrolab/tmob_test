from django.http import JsonResponse, Http404
from django.views.decorators.http import require_http_methods

from redirects.services import RedirectService


@require_http_methods(["GET"])
def index(request):
    result_key = RedirectService.retrieve(request.GET.dict())
    if result_key is None:
        return JsonResponse({"error": "this url isn't active! Go to admin to active it."}, status=404)
    else:
        return JsonResponse(result_key)
