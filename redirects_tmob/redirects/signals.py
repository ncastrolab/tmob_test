import django
from django.dispatch import receiver

toggle_redirect = django.dispatch.Signal(providing_args=["redirect"])
