import json
from datetime import datetime
from unittest.mock import patch

from django.db import IntegrityError
from django.forms import model_to_dict
from django.test import TestCase, Client
from redirects.models import Redirect

from django.core.cache import cache


class BaseClass(TestCase):

    def tearDown(self) -> None:
        cache.clear()


class RedirectModelTestCase(BaseClass):

    def setUp(self):
        self.fixed_date = datetime.now()

    def test_create_redirect(self):
        redirect_a = Redirect.objects.create(
            key="search_engine",
            url="https://www.google.com/",
            active=True
        )

        redirect_a = Redirect.objects.filter(pk=redirect_a.id)
        self.assertIsNotNone(redirect_a)

    @patch('redirects.models.now')
    def test_create_redirect_with_created_at_at_this_moment(self, patched_now):
        patched_now.return_value = self.fixed_date
        redirect_a = Redirect.objects.create(
            key="search_engine",
            url="https://www.google.com/",
            active=True
        )
        self.assertEquals(redirect_a.created_at, self.fixed_date)

    def test_when_changed_model_redirect_updates_his_updated_at_value(self):
        redirect_a = Redirect.objects.create(
            key="search_engine",
            url="https://www.google.com/",
            active=True
        )
        self.assertIsNotNone(redirect_a)

        redirect_from_db = Redirect.objects.filter(pk=redirect_a.id).first()

        first_date = redirect_from_db.updated_at

        redirect_from_db.url = "https://www.facebook.com/"
        redirect_from_db.save()

        second_date = redirect_from_db.updated_at

        redirect_from_db.url = "https://www.twitch.tv/"
        redirect_from_db.save()

        third_date = redirect_from_db.updated_at

        self.assertGreater(second_date, first_date)
        self.assertGreater(third_date, second_date)

    def test_redirect_key_value_is_unique(self):
        Redirect.objects.create(
            key="search_engine",
            url="https://www.google.com/",
            active=True
        )
        with self.assertRaises(IntegrityError, msg='Key is a unique value'):
            Redirect.objects.create(
                key="search_engine",
                url="https://www.google.com/",
                active=True
            )


class RedirectCacheTestCase(BaseClass):

    def test_save_redirect_on_cache(self):
        redirect = Redirect.objects.create(
            key="search_engine",
            url="https://www.google.com/",
            active=True
        )
        cache.set(redirect.key, redirect.custom_values())
        self.assertEquals(cache.get(redirect.key), redirect.custom_values())

    @patch('redirects.services.cache_signal')
    def test_call_signal_handler(self, cache_signal):
        redirect = Redirect.objects.create(
            key="search_engine",
            url="https://www.google.com/",
            active=False
        )
        cache_signal.assert_called_once_with(
            redirect
        )
        redirect.active = True
        redirect.save()
        cache_signal.assert_called_with(
            redirect
        )
        self.assertEquals(2, len(cache_signal.mock_calls))

    def test_when_set_to_active_save_on_cache(self):
        redirect = Redirect.objects.create(
            key="search_engine",
            url="https://www.google.com/",
            active=False
        )

        redirect.active = True
        redirect.save()

        self.assertEquals(cache.get(redirect.key), redirect.custom_values())

    def test_when_set_to_inactive_remove_from_cache(self):
        redirect = Redirect.objects.create(
            key="search_engine",
            url="https://www.google.com/",
            active=True
        )

        self.assertEquals(cache.get(redirect.key), redirect.custom_values())

        redirect.active = False
        redirect.save()
        self.assertIsNone(cache.get(redirect.key))

    def test_when_set_to_inactive_already_inactive_nothing_happend(self):
        redirect = Redirect.objects.create(
            key="search_engine",
            url="https://www.google.com/",
            active=False
        )

        self.assertIsNone(cache.get(redirect.key))

        redirect.active = False
        redirect.save()
        self.assertIsNone(cache.get(redirect.key))


class RedirectViewTestCase(BaseClass):

    def test_return_redirect_for_specific_value(self):
        redirect = Redirect.objects.create(
            key="search_engine",
            url="https://www.google.com/",
            active=True
        )
        self.assertEquals(cache.get(redirect.key), redirect.custom_values())
        c = Client()
        response = c.get('/redirects/?key=search_engine')

        response_json = response.json()
        self.assertEquals(response_json, redirect.custom_values())

    def test_if_not_active_redirect_return_404(self):
        Redirect.objects.create(
            key="search_engine",
            url="https://www.google.com/",
            active=False
        )

        c = Client()
        response = c.get('/redirects/?key=search_engine')

        self.assertEquals(response.status_code, 404)

    @patch('redirects.services.cache')
    def test_if_active_redirect_return_from_cache(self, cache_mock):
        Redirect.objects.create(
            key="search_engine",
            url="https://www.google.com/",
            active=False
        )
        cache_mock.get.return_value = None
        c = Client()
        c.get('/redirects/?key=search_engine')
        cache_mock.get.assert_called_once_with("search_engine")
