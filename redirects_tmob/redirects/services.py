from django.dispatch import receiver

from redirects import signals
from redirects.models import Redirect
from django.core.cache import cache


def cache_signal(to_cache):
    if to_cache.active:
        cache.set(to_cache.key, to_cache.custom_values())
    else:
        cache.delete(to_cache.key)


class RedirectService:
    _model = Redirect

    @classmethod
    def retrieve(cls, params):
        return cache.get(params.get("key"))

    @staticmethod
    @receiver(signals.toggle_redirect)
    def toggle_redirect(sender, redirect, **kwargs):
        cache_signal(redirect)
