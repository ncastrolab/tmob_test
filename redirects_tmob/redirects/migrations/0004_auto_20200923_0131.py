# Generated by Django 2.2 on 2020-09-23 01:31

from django.db import migrations, models
import redirects.utils


class Migration(migrations.Migration):

    dependencies = [
        ('redirects', '0003_auto_20200923_0130'),
    ]

    operations = [
        migrations.AlterField(
            model_name='redirect',
            name='created_at',
            field=models.DateTimeField(default=redirects.utils.now, editable=False),
        ),
    ]
