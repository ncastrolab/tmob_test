# Generated by Django 2.2 on 2020-09-23 01:30

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('redirects', '0002_auto_20200923_0126'),
    ]

    operations = [
        migrations.AlterField(
            model_name='redirect',
            name='created_at',
            field=models.DateTimeField(default=datetime.datetime(2020, 9, 23, 1, 30, 56, 542571, tzinfo=utc), editable=False),
        ),
    ]
