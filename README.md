## Ejercicio tecnico Entrevista Nazareno Castro

###Requirimientos y consideraciones:

> lo ideal es tener docker-compose instalado y un virtualenv 
> utilice docker-compose para poder tener las dbs (mysql y dos memcache una para test y otra para la aplicacion) 
> queda mas limpio el sistema y no me ando peleando con mi maquina que por ahi algo me falta.
> tener en cuenta que para que funcione el conector de python de mysql hay que instalar


`sudo apt-get install libmysqlclient-dev`

##### Para instalar un virtualenv (si es que el que va a revisar esto no sabe como es) dejo un link util

`https://wiki.archlinux.org/index.php/Python_(Espa%C3%B1ol)/Virtual_environment_(Espa%C3%B1ol)`

##### El proyecto esta implementado sobre python 3.6.9

## Como usar el proyecto

Levantar las dbs:

```
cd db_resources
sh run_dbs.sh
```

Instalar dependencias, migrar y correr django



```
# antes entrar en un virtualenv sino esta ensucia el
# env local !!!


pip install -r requirements.txt
cd  redirects_tmob

sh run_server.sh
```

Crear superuser de django para poder entrar al admin

```
# en el virtualenv con todo instalado

cd  redirects_tmob
python manage.py createsuperuser
```

Servicios disponibles


```
localhost:8000/admin #el admin de django

# para traerte la key activa o ver un 404
localhost:8000/redirects/?key=<el_nombre_de_una_key>

```

Para correr los test


> levantar la cache de test
```
cd db_resources
sh run_dbs.sh
```

> correr los test
```
# en el virtualenv con todo instalado

cd  redirects_tmob
sh run_test.sh
```


### Decisiones de diseño

> en general creo que la idea del ejercicio esta cubierta, tal vez algunas cosas que me hubiese gustado agregar (pero tampoco las veo necesarias) seria algun manejo de errores mas generico para los casos que no hay una key en cache y poder testear el comportamiento del django.admin

> Con respecto al enunciado asumi que si un redirect (el objeto del modelo) no esta activo no se va a devolver, es decir, no se va a la db nunca a buscar el objeto excepto cuando se accede al admin.
 
#### Comentario
> si llega a haber algun inconveniente para levantar el proyecto me avisan. Por ahi con docker la complique pero como vengo trabajando asi y no tengo mucho tiempo preferi ir por ese lado (igual me gusto como quedo).